&#x1F34E; > &#x1F449;  点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 


---
### 《Stata直播丨直击面板数据模型 ——公开课》

---
> &#x23E9; 在线[观看](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)：      
> 点击 <https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38>       
> 注册 [连享会直播间](http://lianxh.duanshu.com) 账号后即可免费观看。时长：1小时40分钟。
---

&emsp;

### 嘉宾简介


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君工作照100.JPG)  

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，中山大学岭南学院副教授，博士生导师。已在 **China Economic Review**，**经济研究**，**管理世界**，**金融研究** 等期刊发表论文 60 余篇。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata连享会 (StataChina)」，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 200 万人次。

&emsp; 

### 课程概览

> &#x1F4D9; [直播课展示](https://gitee.com/arlionn/Live)  &rarr; 包含部分课程课件和参考资料

本课程是 [[连玉君-动态面板数据模型 (2.2小时)]](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) 和 [[连玉君-我的甲壳虫-经典论文精讲 (6小时)]](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) 的先导课程，以介绍模型设定思路为主，实操部分请下载 「**Lian_Panel.rar**」压缩包自行演练。

课程中提及的各类模型的实操和综合应用会在 [[连玉君-我的甲壳虫(6小时)]](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) 课程中详细讲解。

此外，若想加强研究设计能力，可以观看 [[连玉君-我的特斯拉-实证研究设计]](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e) (2小时，包含 5 篇论文的重现资料，已经有 1000+ 人学习了该课程)。


&emsp;

#### 为何学习面板数据模型？

目前的实证分析中，基本上都是以「面板数据」为分析对象。好处很明显，一方面，随着样本量的增加，我们的统计推断会更加稳健；另一方面，面板数据同时提供了时序和截面的信息，使得我们既可以分析个体之间的截面差异，也可以分析他们时序动态变化。最为重要的是，使用面板数据还是缓解内生性问题的一个主要方法——我们可以控制那些不可观测的固定效应的影响。

本课程对面板数据模型进行整体简介，包括：固定效应模型 (FE)，随机效应模型 (RE)，二维固定效应模型 (Twoway FE)，聚类调整后的标准误，动态面板和面板门槛模型等。

> ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-直击面板数据二维码.png)    
> 扫码观看视频


### 课程主题

本次直播主要包括如下几个主题：

- 简介：面板数据结构、优势和挑战
- 什么是「固定效应」？辛普森悖论
- 一维和二维固定效应模型
- 估计方法对比分析：POLS，DVLS，Within-FE
- 聚类标准误：一维聚类和多维聚类
- 实证分析中的主要陷阱
- 动态面板和面板门限模型简介

### 获取课程电子包

你可以点击主页右上方的【克隆/下载&rarr;下载zip】，以便下载本仓库的压缩包；也可以申请一个 [码云](https://gitee.com) 账号，然后点击本项目右上角的【Fork】按钮，这样就可以直接把这个仓库「叉」到你的账号下了，随后我这边更新后，你只需要同步一下就可以看到所有的文件了。


> **实操：Stata dofiles/Data/Papers:** 

下载地址：<https://pan.baidu.com/s/1Ri38Xyz_TnFzLNYgsOes4w> (百度云盘，无解压码)。

&emsp; 

&#x1F34E; &#x1F34F;


&emsp;


&emsp;
 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;

## 1. 连享会课程

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/lianxh/Stata33)   
> - [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  

&emsp;

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown]() | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [连享会 · Stata 33 讲 - 免费听](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课件](https://gitee.com/arlionn/stata101)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 

### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/text) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

